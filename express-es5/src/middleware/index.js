const upload = require('./upload.middleware')
const simpleAuth = require('./simpleAuth.middleware')

module.exports = {
    upload,
    simpleAuth
}