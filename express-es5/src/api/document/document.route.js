const express = require('express')
const middleware = require('../../middleware')

const DocumentController = require('./document.controller')

const documentRouter = express.Router()
const documentController = new DocumentController()




documentRouter.route('/testLocalFile').get(documentController.testLocalFile)
documentRouter.route('/extractCSV').post(middleware.upload.single('file'), documentController.extractCSV)

module.exports = documentRouter
