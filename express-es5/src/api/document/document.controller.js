const fs = require('fs')
const response = require('../../../helper/response')
class BookController {
    async testLocalFile(req, res) {
        try {
            // const foundCSV = fs.readFileSync('./document/user123.csv') Jika mau test try catch
            const foundCSV = fs.readFileSync('./document/user.csv')
            const stringCSV = Buffer.from(foundCSV).toString();
            const lines = stringCSV.split('\n');
            const users = []
    
            lines.forEach((line, index) => {
                if (index !== 0) {
                    const newObject = {}
                    const columns = lines[0].split(',')
                    columns.forEach((column, index) => {
                        const columnsValue = line.split(',')
                        newObject[column] = columnsValue[index]
                    })
                    users.push(newObject)
                }
            });
            response.success(res, 200, null, users, "Success Get User.")
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }

    async extractCSV(req, res) {
        try {
            const foundCSV = fs.readFileSync(`./uploads/${req.file.originalname}`)
            const stringCSV = Buffer.from(foundCSV).toString();
            const lines = stringCSV.split('\n');
            const users = []
    
            lines.forEach((line, index) => {
                if (index !== 0) {
                    const newObject = {}
                    const columns = lines[0].split(',')
                    columns.forEach((column, index) => {
                        const columnsValue = line.split(',')
                        newObject[column] = columnsValue[index]
                    })
                    users.push(newObject)
                }
            });
            response.success(res, 200, null, users, "Success Extract User.")
        } catch (error) {
            console.log(error)
            response.errorInternal(res)
        }
    }

}

module.exports = BookController