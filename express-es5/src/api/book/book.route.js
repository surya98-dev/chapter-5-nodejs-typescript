const express = require('express')
const BookController = require('./book.controller')

const bookRouter = express.Router()
const bookController = new BookController()


bookRouter.route('/getAllBooks').get(bookController.getAllBooks)
bookRouter.route('/getBook/:book_id').get(bookController.getBook)
bookRouter.route('/createBook').post(bookController.createBook)
bookRouter.route('/deleteBook/:book_id').get(bookController.deleteBook)

module.exports = bookRouter
