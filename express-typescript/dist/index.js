"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const fs_1 = __importDefault(require("fs"));
const app = express_1.default();
app.get('/', (req, res) => {
    const foundCSV = fs_1.default.readFileSync('./document/user.csv');
    const stringCSV = Buffer.from(foundCSV).toString();
    const lines = stringCSV.split('\n');
    const users = [];
    lines.forEach((line, index) => {
        if (index !== 0) {
            const newObject = {};
            const columns = lines[0].split(',');
            columns.forEach((column, index) => {
                const columnsValue = line.split(',');
                newObject[column] = columnsValue[index];
            });
            users.push(newObject);
        }
    });
    console.log(users);
    res.send('Well done!');
});
app.listen(3001, () => {
    console.log('The application is listening on port 3001!');
});
//# sourceMappingURL=index.js.map